package com.example.testhiring.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(path = "/api/job")
public class JobController {
    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object[]> getListJob() {
        String url = "https://dev6.dansmultipro.com/api/recruitment/positions.json";
        RestTemplate restTemplate = new RestTemplate();
        Object[] jobs = restTemplate.getForObject(url, Object[].class);
        return ResponseEntity.ok(jobs);
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getJob(@PathVariable(value = "id") String id) {
        String url = "https://dev6.dansmultipro.com/api/recruitment/positions/" + id;
        RestTemplate restTemplate = new RestTemplate();
        Object job = restTemplate.getForObject(url, Object.class);
        return ResponseEntity.ok(job);
    }
}
