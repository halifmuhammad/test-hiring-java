package com.example.testhiring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestHiringApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestHiringApplication.class, args);
	}

}
